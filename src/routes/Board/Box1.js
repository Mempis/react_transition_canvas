import React, { Component } from "react";
import PropTypes from "prop-types";
import { Layer, Rect, Stage, Group, Line } from "react-konva";
import konva from "konva";
class Box1 extends Component {
  constructor(props) {
    super(props);
    this.changeSize = this.changeSize.bind(this);
  }
  changeSize() {
    this.refs.line.to({
      points: [
        18,
        300,
        97,
        214,
        160,
        114,
        199,
        103,
        278,
        203,
        343,
        254,
        400,
        300
      ],
      duration: 0.5
    });
  }
  render() {
    return (
      <div style={this.props.style} onClick={this.changeSize}>
        <Stage width={417} height={402}>
          <Layer>
            <Line
              ref="line"
              stroke="#333" // #333, black, pink 등 가능
              strokeWidth={1}
              points={[18, 300, 400, 300]}
              lineCap="round"
              lineJoin="round"
              tension={0.3}
            />
          </Layer>
        </Stage>
      </div>
    );
  }
}

Box1.propTypes = {};

export default Box1;
