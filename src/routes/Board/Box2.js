import React, { Component } from "react";
import PropTypes from "prop-types";
import ExComponent from "./ExComponent";
class Box2 extends Component {
  render() {
    return (
      <div style={this.props.style}>
        2
        <ExComponent />
        <span style={{ border: "1px solid #333" }}>하단 박스</span>
      </div>
    );
  }
}

Box2.propTypes = {};

export default Box2;
