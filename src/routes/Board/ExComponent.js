import React, { Component } from "react";
import PropTypes from "prop-types";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import "./ExComponent.css";
class ExComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };

    this.handleOnClick = this.handleOnClick.bind(this);
  }
  handleOnClick() {
    this.setState({
      show: !this.state.show
    });
  }
  render() {
    const slideStyle = {
      width: 200,
      height: 200,
      border: "1px solid #333",
      position: "absolute"
    };
    const FadeTransition = props => (
      <CSSTransition {...props} classNames="pageSlider" timeout={500} />
    );
    return (
      <div style={this.props.style}>
        <button onClick={this.handleOnClick}>클릭</button>
        <TransitionGroup>
          {this.state.show && (
            <FadeTransition>
              <div style={slideStyle}>슬라이드 될 박스</div>
            </FadeTransition>
          )}
        </TransitionGroup>
      </div>
    );
  }
}

ExComponent.propTypes = {};

export default ExComponent;
