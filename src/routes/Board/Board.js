import React, { Component } from "react";
import PropTypes from "prop-types";
import Box1 from "./Box1";
import Box2 from "./Box2";
class Board extends Component {
  render() {
    const styles = {
      div1: {
        width: "49.5%",
        height: 400,
        float: "left",
        border: "1px solid #333"
      },
      div2: {
        width: "49.5%",
        height: 400,
        float: "right",
        border: "1px solid #333"
      }
    };
    return (
      <div>
        <Box1 style={styles.div1} />
        <Box2 style={styles.div2} />
      </div>
    );
  }
}

Board.propTypes = {};

export default Board;
