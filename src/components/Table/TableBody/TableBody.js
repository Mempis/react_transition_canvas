import React, { Component } from "react";
import PropTypes from "prop-types";

class TableBody extends Component {
  render() {
    return (
      <tbody>
        <TableRow data={this.props.data} tbody />
      </tbody>
    );
  }
}

TableBody.propTypes = {};

export default TableBody;
