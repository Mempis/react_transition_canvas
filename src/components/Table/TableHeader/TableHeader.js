import React, { Component } from "react";
import PropTypes from "prop-types";
import TableRow from "./TableRow/TableRow";
class TableHeader extends Component {
  render() {
    return (
      <thead>
        <TableRow data={this.props.data} thead />
      </thead>
    );
  }
}

TableHeader.propTypes = {};

export default TableHeader;
