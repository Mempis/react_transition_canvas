import React, { Component } from "react";
import PropTypes from "prop-types";

class TableRow extends Component {
  render() {
    const TBody = data => {
      if (!data) return null;

      return data.map((item, i) => {
        if (item.boxClick) {
          return (
            <td
              onClick={() => {
                alert("박스");
              }}
            >
              {item.text}
            </td>
          );
        } else if (item.textClick) {
          return (
            <td>
              <a
                onClick={() => {
                  alert("텍스트");
                }}
              >
                {item.text}
              </a>
            </td>
          );
        }

        return <td>{item.text}</td>;
      });
    };

    const THead = data => {
      if (!data) return null;

      return data.map((item, i) => {
        if (item.boxClick) {
          return (
            <th
              onClick={() => {
                alert("박스");
              }}
            >
              {item.text}
            </th>
          );
        } else if (item.textClick) {
          return (
            <th>
              <a
                onClick={() => {
                  alert("텍스트");
                }}
              >
                {item.text}
              </a>
            </th>
          );
        }

        return <th>{item.text}</th>;
      });
    };
    return (
      <tr>
        {!!this.props.thead ? THead(this.props.data) : TBody(this.props.data)}
      </tr>
    );
  }
}

TableRow.propTypes = {};

export default TableRow;
