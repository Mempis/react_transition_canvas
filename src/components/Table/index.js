import React, { Component } from "react";
import PropTypes from "prop-types";
import TableHeader from "./TableHeader/TableHeader";
import TableBody from "./TableBody/TableBody";
class Table extends Component {
  render() {
    return (
      <div>
        <table>
          <TableHeader />
          <TableBody />
        </table>
      </div>
    );
  }
}

Table.propTypes = {};

export default Table;
