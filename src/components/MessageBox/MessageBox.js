import React, { Component } from "react";
import PropTypes from "prop-types";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import "./MessageBox.css";
import Rnd from "react-rnd";
import icon from "../../images/mailicon.png";
class MessageBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: "small" // big
    };
  }
  render() {
    const styles = {
      container: {
        width: 60,
        height: 60,
        backgroundImage: `url(${icon})`,
        backgroundSize: "cover",
        transition: "300ms"
      },
      container2: {
        border: "1px solid #888",
        borderRadius: 5,
        width: 500,
        height: 500,
        backgroundColor: "#eee",
        transition: "300ms"
      },
      count: {
        display: "table-cell",
        borderRadius: 5,
        width: 30,
        height: 20,
        backgroundColor: "#f00",
        fontSize: 12,
        color: "#fff",
        textAlign: "center",
        verticalAlign: "middle",
        position: "relative",
        top: -5,
        right: -35
      }
    };
    const container =
      this.state.mode === "small" ? styles.container : styles.container2;
    const topCount =
      this.state.mode === "small" ? <div style={styles.count}>23</div> : null;
    return (
      <div style={{ position: "absolute", bottom: 10, right: 10 }}>
        <div
          style={container}
          onClick={() => {
            this.setState({
              mode: this.state.mode === "small" ? "big" : "small"
            });
          }}
        >
          {topCount}
          {this.state.mode === "big" ? "헬로" : null}
        </div>
      </div>
    );
  }
}

MessageBox.propTypes = {};

export default MessageBox;
