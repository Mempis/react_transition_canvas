import React, { Component } from "react";
import "./App.css";
import {
  NavLink,
  Router,
  Route,
  Switch,
  BrowserRouter
} from "react-router-dom";
import MasterPage from "./layouts";
import { Home, Board } from "./routes";

class App extends Component {
  render() {
    const DefaultRoute = ({ component: Component, ...rest }) => {
      return (
        <Route
          {...rest}
          render={matchProps => (
            <MasterPage {...matchProps} {...rest.params}>
              <Component {...matchProps} />
            </MasterPage>
          )}
        />
      );
    };
    return (
      <div>
        <Switch>
          <DefaultRoute exact path="/" component={Home} />
          <DefaultRoute path="/board" component={Board} />
        </Switch>
      </div>
    );
  }
}

export default App;
